#include <Arduino.h>
#include <WiFi.h>
#include <Adafruit_NeoPixel.h>

#define WIFI_SSD "YOUR_WIFI_SSD"
#define WIFI_PASSWORD "YOUR_WIFI_PASSWORD"

#define QUERY_HOST "bin-of-the-week.apps.gjk.me.uk"
#define QUERY_PORT 80
#define QUERY_PATH "/street?street=YOUR+STREET"

#define PIXEL_PIN 23
#define PIXEL_COUNT 12

#define SHOW_SECONDS 60 * 5
#define DEEP_SLEEP_SECONDS 60 * 10
#define TICKS_TO_SECONDS 1000000

enum State {
    START,
    CONNECTING,
    CONNECTED,
    DATA_PENDING,
    DATA_RECEIVED,
    SHOWING,
    FAILED
};

Adafruit_NeoPixel strip = Adafruit_NeoPixel(PIXEL_COUNT, PIXEL_PIN, NEO_GRB + NEO_KHZ800);

uint32_t BLACK = strip.Color(0,0,0);
uint32_t YELLOW = strip.Color(255,255,0);
uint32_t GREEN = strip.Color(0,255,0);
uint32_t BLUE = strip.Color(0,0,255);
uint32_t RED = strip.Color(255,0,0);

State state = START;
void colorWipe(uint32_t c, uint8_t wait) {
  for(uint16_t i=0; i<strip.numPixels(); i++) {
    strip.setPixelColor(i, c);
    strip.show();
    delay(wait);
  }
}

void all(uint32_t c) {
  for(uint16_t i=0; i<strip.numPixels(); i++) {
    strip.setPixelColor(i, c);
  }
  strip.show();
}

void single(uint32_t colour, int pixel, boolean show) {
    if (pixel >= 0 && pixel < PIXEL_COUNT) {
        strip.setPixelColor(pixel, colour);
    }
    if (show) {
        strip.show();
    }
}

void allOff() {
    all(strip.Color(0, 0, 0));
}

void configure_pins() {
    strip.begin();
    strip.show();
}

void setup() {
    Serial.begin(115200);
    configure_pins();
}

int start_time;
int strobe_id = -1;

void setState(State value) {
    strobe_id = -1;
    all(strip.Color(0,0,0));
    start_time = millis();
    state = value;
}

void start() {
    WiFi.begin(WIFI_SSD, WIFI_PASSWORD);
    setState(CONNECTING);
}

void strobe(int time, uint32_t colour) {
    single(BLACK, strobe_id, false);
    strobe_id = (strobe_id + 1) % PIXEL_COUNT;
    single(colour, strobe_id, true);
    delay(time);
}

void connecting() {
    if (WiFi.status() == WL_CONNECT_FAILED) {
        Serial.println("Failed to connect to wifi");
        setState(FAILED);
    } else if (WiFi.status() == WL_CONNECTED) {
        all(YELLOW);
        setState(CONNECTED);
    } else {
        strobe(200, YELLOW);
    }
}

void failed() {
    if (millis() >= start_time + 10000) {
        esp_sleep_enable_timer_wakeup(DEEP_SLEEP_SECONDS * TICKS_TO_SECONDS);
        esp_deep_sleep_start();
    } else {
        strobe(1000, RED);
    }
}

WiFiClient client;

void connected() {
    if (client.connect(QUERY_HOST, QUERY_PORT)) {
        char query[256];
        sprintf(query, "GET %s HTTP/1.1\r\nHost: %s\r\nConnection: close\r\n\r\n", QUERY_PATH, QUERY_HOST);
        client.print(query);
        setState(DATA_PENDING);
    } else {
        Serial.println("Failed to connect to host");
        setState(FAILED);
    }
}

String response;

void dataPending() {
    if (client.available() > 0) {
        String line = client.readStringUntil('\r');
        Serial.printf("Got data: %s\n", line.c_str());
        response += line;
    } else if (!client.connected()) {
        client.stop();
        setState(DATA_RECEIVED);
    } else if (millis() > start_time + 30000) {
        client.stop();
        Serial.println("Query timed out after 30 seconds");
        setState(FAILED);
    } else {
        strobe(150, YELLOW);
    }
}

void dataReceived() {
    if (response.indexOf("green") >= 0) {
        setState(SHOWING);
        colorWipe(GREEN, 100);
    } else if (response.indexOf("blue") >= 0) {
        setState(SHOWING);
        colorWipe(BLUE, 100);
    } else {
        Serial.printf("Unexpected response: %s", response.c_str());
        setState(FAILED);
    }
}

int level = 255;
boolean down = true;

void showing() {
    if (millis() >= start_time + (SHOW_SECONDS * 1000)) {
        esp_sleep_enable_timer_wakeup(DEEP_SLEEP_SECONDS * TICKS_TO_SECONDS);
        esp_deep_sleep_start();
    } else {
        strip.setBrightness(level);
        strip.show();
        if (down) {
            if (level <= 64) {
                down = false;
            } else {
                level -= 10;
            }
        } else {
            if (level >= 250) {
                down = true;
            } else {
                level += 10;
            }
        }

        delay(100);
    }
}

void loop() {
    switch(state) {
        case START: start(); break;
        case CONNECTING: connecting(); break;
        case CONNECTED: connected(); break;
        case DATA_PENDING: dataPending(); break;
        case DATA_RECEIVED: dataReceived(); break;
        case SHOWING: showing(); break;
        case FAILED: failed(); break;
    }
}