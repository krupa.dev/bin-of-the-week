package uk.me.gjk.bins.date

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.containsInAnyOrder
import org.hamcrest.Matchers.empty
import org.junit.jupiter.api.Test
import java.time.LocalDate

class NextApplicableDateCalculatorTest {

    private val subjectUnderTest = NextApplicableDateCalculator()

    private val dateSelections = mapOf<LocalDate, List<String>>(
            LocalDate.of(2018, 8, 23) to listOf("A", "B"),
            LocalDate.of(2018, 8, 25) to listOf("B", "C"),
            LocalDate.of(2018, 8, 26) to listOf("C", "D")
    )

    @Test
    fun `calculate next date from valid day results in the same day`() {
        assertThat(subjectUnderTest.next(dateSelections, LocalDate.of(2018, 8, 25)),
                containsInAnyOrder("B", "C"))
    }

    @Test
    fun `calculate next day from day before results in tomorrow`() {
        assertThat(subjectUnderTest.next(dateSelections, LocalDate.of(2018, 8, 24)),
                containsInAnyOrder("B", "C"))
    }

    @Test
    fun `calculate next day from last day in set results in same day`() {
        assertThat(subjectUnderTest.next(dateSelections, LocalDate.of(2018, 8, 26)),
                containsInAnyOrder("C", "D"))
    }

    @Test
    fun `calculate next day 6 days before the first day results in first day`() {
        assertThat(subjectUnderTest.next(dateSelections, LocalDate.of(2018, 8, 17)),
                containsInAnyOrder("A", "B"))
    }

    @Test
    fun `calculate next day 7 days before the first day results in no match`() {
        assertThat(subjectUnderTest.next(dateSelections, LocalDate.of(2018, 8, 16)),
                empty())
    }

    @Test
    fun `calculate next day after the last day results in no match`() {
        assertThat(subjectUnderTest.next(dateSelections, LocalDate.of(2018, 8, 27)),
                empty())
    }
}
