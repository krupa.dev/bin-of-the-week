package uk.me.gjk.bins.parser

import org.hamcrest.Matchers.*
import org.hamcrest.MatcherAssert.assertThat
import org.junit.jupiter.api.Test
import java.time.LocalDate

class HtmlScheduleParserTests {
    @Test
    fun `Parsing Thursday produces a list of bin colours and dates`() {
        val parser = HtmlParser()
        val results = parser.parse(javaClass.classLoader.getResourceAsStream("thursday.html"))
        assertThat(results, not(nullValue()))
        assertThat(results[LocalDate.of(2018, 8, 2)], containsInAnyOrder("green"))
        assertThat(results[LocalDate.of(2018, 8, 9)], containsInAnyOrder("blue", "brown"))
        assertThat(results[LocalDate.of(2018, 8, 16)], containsInAnyOrder("green"))
        assertThat(results[LocalDate.of(2018, 8, 23)], containsInAnyOrder("blue", "brown"))
        assertThat(results[LocalDate.of(2018, 8, 30)], containsInAnyOrder("green"))
        assertThat(results[LocalDate.of(2018, 9, 6)], containsInAnyOrder("blue", "brown"))
        assertThat(results[LocalDate.of(2018, 9, 13)], containsInAnyOrder("green"))
        assertThat(results[LocalDate.of(2018, 9, 20)], containsInAnyOrder("brown", "blue"))
        assertThat(results[LocalDate.of(2018, 9, 27)], containsInAnyOrder("green"))
        assertThat(results[LocalDate.of(2018, 9, 26)], nullValue())
    }

}
