package uk.me.gjk.bins.parser

import org.jsoup.Jsoup
import org.springframework.stereotype.Component
import java.io.InputStream
import java.time.LocalDate
import java.time.format.DateTimeFormatter

@Component
class HtmlParser {

    private val formatter: DateTimeFormatter = DateTimeFormatter.ofPattern("d MMMM uuuu")

    fun parse(stream: InputStream?): Map<LocalDate,List<String>> {
        val jsoup = Jsoup.parse(stream, "UTF-8", "http://localhost")
        return jsoup.body()
                .select("h2")
                .filter { it.text().matches("[A-Z][a-z]+ [0-9]{4}".toRegex()) }
                .flatMap {
                    val year = it.text().replace("[A-Z][a-z]+ ".toRegex(), "").toInt()
                    val days = it.parent().select("ul>li")
                            .map {
                                val date = it.text()
                                        .replace("^([A-Z][a-z]+)\\s+([0-9]+)\\s+([A-Z][a-z]+).*$".toRegex(), "$2 $3 $year")
                                val colours = it.select("a")
                                        .map { it.text() }
                                        .filter { it.contains("-lidded") }
                                        .map { it.substringBefore('-') }

                                LocalDate.parse(date, formatter) to colours

                            }
                    days
                }.toMap()
    }
}