package uk.me.gjk.bins

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.scheduling.annotation.EnableScheduling

@SpringBootApplication
@EnableScheduling
class BinOfTheWeekApplication

fun main(args: Array<String>) {
    runApplication<BinOfTheWeekApplication>(*args)
}


