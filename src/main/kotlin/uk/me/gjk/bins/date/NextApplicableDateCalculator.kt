package uk.me.gjk.bins.date

import mu.KotlinLogging
import org.springframework.stereotype.Component
import java.time.LocalDate
import java.util.*
import java.util.function.Consumer
import java.util.stream.StreamSupport

private val log = KotlinLogging.logger {  }

@Component
class NextApplicableDateCalculator {

    fun next(dateSelection: Map<LocalDate,List<String>>, today: LocalDate): List<String> {
        val dateStream = StreamSupport.stream(DateSpliterator(today), false)

        return dateStream
                .limit(7)
                .filter { dateSelection.containsKey(it) }
                .peek { log.info { it.toString() } }
                .findFirst()
                .map { dateSelection[it].orEmpty() }
                .orElse(emptyList())
    }

    class DateSpliterator(private var lastDate: LocalDate) : Spliterator<LocalDate> {

        override fun estimateSize(): Long {
            return 0
        }

        override fun characteristics(): Int {
            return 0
        }

        override fun tryAdvance(action: Consumer<in LocalDate>): Boolean {
            action.accept(lastDate)
            lastDate = lastDate.plusDays(1)
            return true
        }

        override fun trySplit(): Spliterator<LocalDate> {
            return this
        }

    }
}
