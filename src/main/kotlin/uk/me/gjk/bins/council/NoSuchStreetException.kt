package uk.me.gjk.bins.council

class NoSuchStreetException(message: String): Exception(message)