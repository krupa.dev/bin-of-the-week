package uk.me.gjk.bins.council

import mu.KotlinLogging
import org.jsoup.Jsoup
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.MediaType
import org.springframework.http.client.reactive.ReactorClientHttpConnector
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.util.DefaultUriBuilderFactory
import org.springframework.web.util.UriComponentsBuilder
import reactor.core.publisher.Mono
import reactor.netty.http.client.HttpClient
import uk.me.gjk.bins.parser.HtmlParser
import java.io.ByteArrayInputStream
import java.time.LocalDate

private val log = KotlinLogging.logger {  }

@Component
class CouncilClient(@Value("\${council.search.url}") val searchUrl: String, @Value("\${council.search.directory}") val directoryId: Int, val htmlParser: HtmlParser) {

    companion object {
        val URI_BUILDER_FACTORY = DefaultUriBuilderFactory()
    }

    val httpClient = HttpClient.create().followRedirect(true)

    val webClient = WebClient.builder()
        .clientConnector(ReactorClientHttpConnector(httpClient))
        .build()

    fun pageForStreetName(streetName: String): Mono<String> {
        return detailsPageForStreetName(streetName)
                .flatMap { collectionPageForDetailsUrl(it) }
    }

    fun detailsPageForStreetName(streetName: String): Mono<String> {
        val builder = URI_BUILDER_FACTORY.uriString(searchUrl)
        return webClient.get()
                .uri(builder
                        .queryParam("directoryID", directoryId)
                        .queryParam("keywords", streetName)
                        .queryParam("search", "Go")
                        .build()
                )
                .accept(MediaType.TEXT_HTML)
                .retrieve()
                .onStatus({ it.isError }, { throw NoSuchStreetException("No such street found") })
                .bodyToMono(String::class.java)
                .map { extractStreetUrl(it) }
    }

    fun datesForCollectionUrl(url: String): Mono<Map<LocalDate, List<String>>> {
        return webClient.get()
                .uri(url)
                .accept(MediaType.TEXT_HTML)
                .retrieve()
                .onStatus({ it.isError }, { throw NoSuchStreetException("Page was not found") })
                .bodyToMono(String::class.java)
                .map { ByteArrayInputStream(it.toByteArray()) }
                .map(htmlParser::parse)
    }

    private fun collectionPageForDetailsUrl(detailsPageUrl: String): Mono<String> {
        return webClient.get()
                .uri(detailsPageUrl)
                .accept(MediaType.TEXT_HTML)
                .retrieve()
                .onStatus({ it.isError }, { throw NoSuchStreetException("Details page did not exist") })
                .bodyToMono(String::class.java)
                .map { extractCollectionUrl(it) }
    }

    private fun extractStreetUrl(htmlBody: String): String =
        Jsoup.parse(htmlBody, searchUrl)
                .body()
                .select("ul.item-list>li>a")
                .stream()
                .map { it.attr("href") }
                .map { UriComponentsBuilder.fromUriString(searchUrl).replacePath(it).build().toUriString() }
                .findFirst()
                .orElseThrow { NoSuchStreetException("No such street found") }

    private fun extractCollectionUrl(htmlBody: String): String =
        Jsoup.parse(htmlBody, searchUrl)
                .body()
                .select("div[class='editor']>p>a")
                .stream()
                .map { it.attr("href") }
                .peek(log::info)
                .findFirst()
                .orElseThrow { NoSuchStreetException("Details page did not contain a collection page link") }

}
