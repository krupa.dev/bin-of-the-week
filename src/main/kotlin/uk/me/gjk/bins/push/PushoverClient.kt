package uk.me.gjk.bins.push

import org.springframework.beans.factory.annotation.Value
import org.springframework.http.MediaType
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.BodyInserters
import org.springframework.web.reactive.function.client.WebClient

@Component
class PushoverClient(
    @Value("\${pushover.username}") private val username: String,
    @Value("\${pushover.token}") private val token: String
) {

    val webClient = WebClient.builder().build()

    fun sendMessage(message: String) =
        webClient.post()
            .uri("https://api.pushover.net/1/messages.json")
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .body(BodyInserters.fromFormData("token", token)
                .with("user", username)
                .with("message", message)
            )
            .retrieve()
            .bodyToMono(String::class.java)
            .onErrorMap { IllegalStateException(it) }
}
